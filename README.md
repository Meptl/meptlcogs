# MeptlCogs
Cogs made for [Red-DiscordBot](https://github.com/Cog-Creators/Red-DiscordBot)

To add this repo to your bot: `[p]repo add MeptlCogs https://gitlab.com/Meptl/MeptlCogs/`

To install specific cogs: `[p]cog install MeptlCogs COGNAME`

To load the cog: `[p]load COGNAME`

## Cogs

| Name | Description |
| --- | --- |
| chatgpt | Interface to [OpenAI's ChatGPT](https://openai.com/blog/chatgpt/)


## Development

This setup uses the latest Red-DiscordBot source code mounted into a docker container
that runs the bot.
Cogs are also placed on this mounted volume.
Unfortunately, because the Cog source files must be available to the container
the Cog repo is also within the mounted volume.


Define an `env_file` that looks like:

```
PREFIX=.
DISCORD_TOKEN=REDACTED
TOKEN=$DISCORD_TOKEN
CUSTOM_REDBOT_PACKAGE=git+file:///data/Red-DiscordBot
```

Clone the latest copy of Red-DiscordBot into a persistence directory and use docker to run an instance of the bot.

```
mkdir red-persistence
git clone https://github.com/Cog-Creators/Red-DiscordBot red-persistence/Red-DiscordBot
docker run -v $(pwd)/red-persistence:/data --env-file=env_file phasecorex/red-discordbot
```

Place cogs in active development into Red's cog folder.
In this case, `red-persistence/cogs/CogManager/cogs/`.

You can then `[p]reload COG`.
For quick testing, you can alias commands: `[p]alias add r reload chatgpt`
