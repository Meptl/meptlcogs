import os

from .chatgpt.httpserver import HttpServer as ChatGPTBackend
from .chatgptcog import ChatGPTCog


async def setup(bot):
    gpt_endpoint = os.getenv("OPENAI_GPT_ENDPOINT")
    await bot.add_cog(ChatGPTCog(bot, ChatGPTBackend(endpoint=gpt_endpoint)))
