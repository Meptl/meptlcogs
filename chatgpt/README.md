# ChatGPT

A cog for interfacing with [OpenAI's ChatGPT](https://openai.com/blog/chatgpt/).

This cog uses Discord thread's to maintain conversation information.
As a result, multiple users can partake in a single conversation with ChatGPT.
Additionally, conversation memory is separate per Discord thread.

Currently, ChatGPT is a moving target and doesn't have an official API, so this cog targets a server that acts as a proxy for ChatGPT's API.
The communication protocol is defined in [the Interface section](#interface).

Currently, there is no commands to configure the cog at runtime,
so you must define the target server at Red's startup time.
Threads managed by this cog are automatically closed and locked after (about) 10 minutes.

## Requirements
This cog requires the ability to create Discord threads.
At the time of writing, this is not available on the current stable release of Red-DiscordBot (v3.4.18).
```
Red-DiscordBot >= 3.5.0
```

## Installation
Define the OPENAI_GPT_ENDPOINT environment variable before starting Red.
```
export OPENAI_GPT_ENDPOINT=http://localhost:3000
```

## Interface
This cog communicates with an external server using JSON:
```
{
  "message": "Hi ChatGPT! How would I parse JSON with a bash command?",
  "conversationId": null,
  "parentMessageId": null,
}
```
No authenticate keys are sent. It expects a response in JSON:
```
{
  "message": "To parse JSON with a bash command, you can use the `jq` tool. `jq` is a command-line JSON processor that allows you to parse, filter, and transform JSON data.\n\nHere's an example of how you can use `jq` to parse a JSON file called `data.json`:\n\n```bash\njq . data.json\n```\n\nThis will print the entire JSON object to the terminal.\n\nYou can also use `jq` to extract specific values from the JSON object. For example, if the JSON object has a field called \"name,\" you can extract the value of that field like this:\n\n```bash\njq .name data.json\n```\n\nYou can also use `jq` to transform the JSON data. For example, you can use the `map()` function to apply a transformation to each element in an array, or use the `select()` function to filter the data based on a condition.\n\nHere's an example of using `jq` to extract the \"name\" field for each object in an array:\n\n```bash\njq 'map(.name)' data.json\n```\n\nFor more information on how to use `jq`, you can check out the documentation at https://stedolan.github.io/jq/.\n",
  "conversationId": "20196da9-0dec-4d83-b75a-15139f824c84",
  "parentMessageId": "a28b980f-6049-4194-a469-a12bf9299238"
}
```
