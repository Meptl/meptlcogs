import json
import logging
import time

from discord.ext import tasks
from redbot.core import commands


log = logging.getLogger("red.chatgptcog")


class ChatGPTCog(commands.Cog):
    def __init__(self, bot, chatgpt, thread_expiration=60*10):
        self.bot = bot
        self.conversations = {}
        self.chatgpt = chatgpt
        self.thread_expiration = thread_expiration


    def cog_unload(self):
        self.prune_old_threads.cancel()


    @tasks.loop(seconds=60*4)
    async def prune_old_threads(self):
        for thread_id in self.conversations.keys():
            if time.time() - self.conversations[thread_id]["last_message_time"] <= self.thread_expiration:
                continue

            thread = await self.bot.fetch_channel(thread_id)
            # Seems the thread is gone
            if not thread:
                del self.conversations[thread_id]
                continue

            expiration_str = self.__human_readable_seconds(self.thread_expiration)
            await thread.send(f"It's been more than {expiration_str} since this thread has received activity.")
            await self.__thread_delete(thread)


    @commands.command()
    async def askgpt(self, ctx):
        async with ctx.channel.typing():
            # Drop the first word which is the command
            prompt = ctx.message.content.split(" ", 1)[1]
            log.info("User prompted: " + prompt)
            response = await self.chatgpt.ask(prompt)
            if not response["conversationId"]:
                # Something went wrong, don't produce a thread, just send the message
                await ctx.send(response["message"])
            else:
                thread = await ctx.message.create_thread(name=ctx.message.content)

                log.info("Response: " + json.dumps(response))
                await thread.send(response["message"])
                self.conversations[thread.id] = {
                    "conversation_id": response["conversationId"],
                    "parent_id": response["parentMessageId"],
                    "last_message_time": time.time(),
                    "working": False
                }


    @commands.Cog.listener()
    async def on_message(self, message):
        # Ignore messages from the bot itself
        if message.author.id == self.bot.user.id:
            return

        # There doesn't seem to be thread data within messages.
        # So search the latest message in each tracked thread.
        for thread_id in self.conversations.keys():
            thread = await self.bot.fetch_channel(thread_id)
            thread_info = self.conversations[thread_id]
            # Seems the thread is gone
            if not thread:
                del self.conversations[thread_id]
                continue

            if thread.last_message_id == message.id:
                if message.content.lower() == "bye":
                    await thread.send("Bye!")
                    await self.__thread_delete(thread)
                    return

                thread_info["last_message_time"] = time.time()

                if thread_info["working"]:
                    await thread.send("I'm still working on your last message.")
                    return

                log.info("User prompted: " + message.content)
                self.conversations[thread_id]["working"] = True
                async with thread.typing():
                    response = await self.chatgpt.ask(
                        message.content,
                        conversation_id=self.conversations[thread_id]["conversation_id"],
                        parent_id=self.conversations[thread_id]["parent_id"]
                    )
                    log.info("Response: " + json.dumps(response))
                    await thread.send(response["message"])
                    self.conversations[thread_id]["conversation_id"] = response["conversationId"]
                    self.conversations[thread_id]["parent_id"] = response["parentMessageId"]
                self.conversations[thread_id]["working"] = False


    def __human_readable_seconds(self, seconds):
        if seconds < 60:
            return f"{seconds} seconds"
        elif seconds % 60 != 0:
            return f"{seconds // 60} minutes and {seconds % 60} seconds"
        else:
            return f"{seconds // 60} minutes"


    async def __thread_delete(self, thread):
        await thread.edit(archived=True, locked=True)
        del self.conversations[thread.id]
