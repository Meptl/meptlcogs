from json.decoder import JSONDecodeError

import httpx

from . import ChatGPT


class HttpServer(ChatGPT):
    def __init__(self, endpoint="http://localhost:3000"):
        self.endpoint = endpoint


    async def ask(self, prompt, conversation_id=None, parent_id=None):
        async with httpx.AsyncClient() as client:
            try:
                response = await client.post(
                    self.endpoint,
                    json={
                        "message": prompt,
                        "conversationId": conversation_id,
                        "parentMessageId": parent_id
                    },
                    headers={"Content-Type": "application/json"},
                    timeout=180
                )
            except TimeoutError:
                return {
                    "message": "Sorry, I timed out!",
                    "conversationId": None,
                    "parentMessageId": None
                }
        try:
            return response.json()
        except JSONDecodeError:
            return {
                "message": "I failed to query the server: " + response.text,
                "conversationId": None,
                "parentMessageId": None
            }
