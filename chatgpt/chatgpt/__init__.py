from abc import ABC
from abc import abstractmethod


class ChatGPT(ABC):
    @abstractmethod
    async def ask(self, prompt, conversation_id=None, parent_id=None):
        pass
